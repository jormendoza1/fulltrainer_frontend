import { CursosService } from './../../../services/cursos.service';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource: any;
  cursoForm: FormGroup;
  selected: any;

  subrubros: any = [
    {
      id_subrubro: '1',
      nombre: 'FUTBOL'
    },
    {
      id_subrubro: '2',
      nombre: 'BASQUET'
    },
    {
      id_subrubro: '3',
      nombre: 'NATACION'
    }

  ];

  constructor(private cursosService: CursosService, private formBuilder: FormBuilder) { }


  ngOnInit(): void {
    this.selected = this.subrubros[0];
    this.cursoForm = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      publico_destinado: ['', [Validators.required]],
      requisitos: ['', [Validators.required]],
      url_imagen_presentacion: ['', [Validators.required]],
      url_video_presentacion: ['', [Validators.required]],
      precio_inscripcion: ['', [Validators.required]],
      precio_cuota: ['', [Validators.required]],
      cantidad_cuotas: ['', [Validators.required]],
      subrubro: ['']

    });

  }




  listar() {
    try {
      this.cursosService.getCursos().subscribe( resp => {
        console.log(resp);
      });
    } catch {
      console.log('');
    }





  }

  nuevo() {
    console.log(this.cursoForm.value);
    console.log(this.selected);


    let curso = {
        nombre: this.cursoForm.get('nombre').value,
        descripcion: this.cursoForm.get('descripcion').value,
        publico_destinado: this.cursoForm.get('publico_destinado').value,
        requisitos: this.cursoForm.get('requisitos').value,
        url_imagen_presentacion: this.cursoForm.get('url_imagen_presentacion').value,
        url_video_presentacion: this.cursoForm.get('url_video_presentacion').value,
        precio_inscripcion: this.cursoForm.get('precio_inscripcion').value,
        precio_cuota: this.cursoForm.get('precio_cuota').value,
        cantidad_cuotas: this.cursoForm.get('cantidad_cuotas').value,
        id_subrubros: this.selected.id_subrubro
    };
    console.log(curso);

    this.cursosService.postCursos(curso).subscribe( resp => {
        console.log(resp);
    });



  }


}
